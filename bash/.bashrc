#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Enable vi keybindings
set -o vi

# Enable extended globbing
shopt -s extglob

# Include .bashrc_local
if [ -f ~/.bashrc_local ]; then
	source ~/.bashrc_local
fi

# Start ssh-agent keychain
eval $(keychain --eval id_rsa)

alias sudo='sudo '
alias ls='ls --color=auto'
alias info='info --vi-keys'
alias netctl-restart='sudo systemctl restart netctl-auto@wlp2s0b1.service'

man() {
    env \
    LESS_TERMCAP_mb=$'\e[01;31m' \
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    man "$@"
}

PS1='\[\e[31;1m\]$(exit=$?; [[ $exit -ne 0 ]] && echo "$exit ")\[\e[34;1m\]\u\[\e[00m\]@\[\e[32;1m\]\h\[\e[00m\]:\[\e[33;1m\]\w\[\e[00m\]$'
PATH=~/bin:$PATH
TERM=xterm-256color
