" Include vim defaults
unlet! skip_defaults_vim
" source $VIMRUNTIME/defaults.vim

" Vim-Plug
if !filereadable(expand("~/.vim/autoload/plug.vim"))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif
call plug#begin()
Plug 'scrooloose/nerdtree'
" Plug 'valloric/youcompleteme'
Plug 'nanotech/jellybeans.vim'
Plug 'tomtom/tcomment_vim'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'conradirwin/vim-bracketed-paste'
" Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
" Plug 'maralla/validator.vim'
Plug 'ntpeters/vim-better-whitespace'
call plug#end()

" Syntax highlighting
filetype plugin on
syntax on

" Spell checking
autocmd FileType rst,md,latex,gitcommit setlocal spell

" Enable mouse support.
set mouse=a

" Clipboard
set clipboard=unnamedplus
xnoremap p pgvy

" Folding
set foldmethod=indent
set foldlevelstart=99

" Tabs
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent

" UI
set laststatus=2
set relativenumber
set number
set noshowmode
set t_Co=16
colorscheme jellybeans
highlight LineNr ctermfg=White ctermbg=Black

" Move between windows
nmap <C-j> <C-W>j
nmap <C-k> <C-W>k
nmap <C-h> <C-W>h
nmap <C-l> <C-W>l

" Validator
let g:validator_permament_sign = 1

" Airline
let g:airline_theme = 'bubblegum'
let g:airline#extensions#whitespace#mixed_indent_algo = 2

" NERDTree
autocmd vimenter * NERDTree %
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd VimEnter * wincmd p

" YouCompleteMe
" let g:ycm_server_python_interpreter = '/usr/bin/python'
" let g:ycm_add_preview_to_completeopt = 0
" set completeopt-=preview
