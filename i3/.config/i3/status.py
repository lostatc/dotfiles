#!/usr/bin/env python3

from i3pystatus import Status, get_module
from i3pystatus.updates import pacman, dnf

status = Status()

# Dependencies:
# xbacklight
# https://pypi.python.org/pypi/colour
# https://pypi.python.org/pypi/psutil
# https://pypi.python.org/pypi/i3ipc


@get_module
def switch_format(self, formats):
    """Switch the format to the next one in the list."""
    for i, format in enumerate(formats):
        if self.format == format:
            self.format = formats[(i+1) % len(formats)]
            break


@get_module
def update_battery_status(self):
    """Change the battery status based on the charge percentage."""
    if self.data["percentage"] > 80:
        self.status["DIS"] = ""
    elif self.data["percentage"] > 60:
        self.status["DIS"] = ""
    elif self.data["percentage"] > 40:
        self.status["DIS"] = ""
    elif self.data["percentage"] > 20:
        self.status["DIS"] = ""
    else:
        self.status["DIS"] = ""


@get_module
def update_volume_format(self):
    """Change the volume icon based on the volume percentage."""
    if self.data["volume"] > 50:
        self.format = " {volume}%"
    elif 0 < self.data["volume"] < 50:
        self.format = " {volume}%"
    elif self.data["volume"] == 0:
        self.format = " {volume}%"


@get_module
def update_cpu_color(self):
    """Change the cpu color based on the cpu usage."""
    if self.data["usage"] > 80:
        self.color = "#825d4d"
    elif self.data["usage"] > 60:
        self.color = "#ada16d"
    else:
        self.color = None


@get_module
def limited_increase_volume(self):
    """Increase the volume up to 100%."""
    if self.data["volume"] <= 100 - self.step:
        self.increase_volume()


status.register(
    "text",
    text="",
    on_leftclick="lxqt-leave",
    hints={"separator": False})

status.register(
    "clock",
    format=" %l:%M %p",
    hints={"separator": False})

status.register(
    "clock",
    format=" %D",
    hints={"separator": False})

status.register(
    "pulseaudio",
    format=" {volume}%",
    format_muted=" mute",
    color_muted="#4d7b82",
    #on_upscroll=limited_increase_volume,
    on_change=update_volume_format,
    hints={"separator": False})

status.register(
    "battery",
    format="{status} {percentage:.0f}%",
    interval=1,
    status={
        "DPL": "",
        "DIS": "",
        "CHR": "",
        "FULL": ""},
    full_color=None,
    charging_color=None,
    critical_color="#825d4d",
    on_rightclick=[
        switch_format, [
            "{status} {percentage:.0f}%",
            "{status} {remaining:%h:%M}"]],
    on_change=update_battery_status,
    hints={"separator": False})

status.register(
    "backlight",
    format="☀ {percentage}%",
    interval=1,
    hints={"separator": False})

# status.register(
#     "updates",
#     format=" {count}",
#     color=None,
#     interval=900,
#     format_no_updates=" 0",
#     backends=[pacman.Pacman(), dnf.Dnf()],
#     hints={"separator": False})

status.register(
    "cmus",
    format="{status} '{title}'",
    format_not_running="",
    status={
        "playing": "",
        "paused": "",
        "stopped": ""})

status.register(
    "cpu_usage",
    format=" {usage:2}%",
    on_change=update_cpu_color,
    hints={"separator": False})

status.register(
    "mem",
    format=" {used_mem}G/{total_mem}G",
    interval=1,
    divisor=1024**3,
    warn_percentage=60,
    alert_percentage=80,
    warn_color="#ada16d",
    alert_color="#825d4d",
    color=None,
    hints={"separator": False})

status.register(
    "scratchpad",
    format=" {number}",
    color_urgent="#825d4d",
    hints={"separator": False})

status.run()
