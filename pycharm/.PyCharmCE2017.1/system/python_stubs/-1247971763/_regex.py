# encoding: utf-8
# module _regex
# from /usr/lib/python3.6/site-packages/_regex.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc
# no imports

# Variables with simple values

CODE_SIZE = 4

copyright = ' RE 2.3.0 Copyright (c) 1997-2002 by Secret Labs AB '

MAGIC = 20100116

# functions

def compile(*args, **kwargs): # real signature unknown
    pass

def fold_case(*args, **kwargs): # real signature unknown
    pass

def get_all_cases(*args, **kwargs): # real signature unknown
    pass

def get_code_size(*args, **kwargs): # real signature unknown
    pass

def get_expand_on_folding(*args, **kwargs): # real signature unknown
    pass

def get_properties(*args, **kwargs): # real signature unknown
    pass

def has_property_value(*args, **kwargs): # real signature unknown
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is ''

__spec__ = None # (!) real value is ''

