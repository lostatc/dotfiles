# encoding: utf-8
# module psutil._psutil_posix calls itself psutil_posix
# from /usr/lib/python3.6/site-packages/psutil/_psutil_posix.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc
# no imports

# functions

def getpriority(*args, **kwargs): # real signature unknown
    """ Return process priority """
    pass

def net_if_addrs(*args, **kwargs): # real signature unknown
    """ Retrieve NICs information """
    pass

def net_if_flags(*args, **kwargs): # real signature unknown
    """ Retrieve NIC flags """
    pass

def net_if_mtu(*args, **kwargs): # real signature unknown
    """ Retrieve NIC MTU """
    pass

def setpriority(*args, **kwargs): # real signature unknown
    """ Set process priority """
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is ''

__spec__ = None # (!) real value is ''

