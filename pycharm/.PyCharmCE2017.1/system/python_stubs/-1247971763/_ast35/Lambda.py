# encoding: utf-8
# module _ast35
# from /usr/lib/python3.6/site-packages/_ast35.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc
# no imports

from .expr import expr

class Lambda(expr):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    _fields = (
        'args',
        'body',
    )


