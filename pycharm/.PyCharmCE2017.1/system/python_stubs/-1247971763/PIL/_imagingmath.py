# encoding: utf-8
# module PIL._imagingmath
# from /usr/lib/python3.6/site-packages/PIL/_imagingmath.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc
# no imports

# Variables with simple values

abs_F = 139660653149968
abs_I = 139660653147504

add_F = 139660653150160
add_I = 139660653147696

and_I = 139660653148512

diff_F = 139660653150560
diff_I = 139660653148288

div_F = 139660653150448
div_I = 139660653148032

eq_F = 139660653150864
eq_I = 139660653149296

ge_F = 139660653151504
ge_I = 139660653149856

gt_F = 139660653151376
gt_I = 139660653149744

invert_I = 139660653148416

le_F = 139660653151248
le_I = 139660653149632

lshift_I = 139660653148848

lt_F = 139660653151120
lt_I = 139660653149520

max_F = 139660653150768
max_I = 139660653149184

min_F = 139660653150672
min_I = 139660653149072

mod_F = 139660653151904
mod_I = 139660653148160

mul_F = 139660653150352
mul_I = 139660653147920

neg_F = 139660653150064
neg_I = 139660653147600

ne_F = 139660653150992
ne_I = 139660653149408

or_I = 139660653148624

pow_F = 139660653152144
pow_I = 139660653151632

rshift_I = 139660653148960

sub_F = 139660653150256
sub_I = 139660653147808

xor_I = 139660653148736

# functions

def binop(*args, **kwargs): # real signature unknown
    pass

def unop(*args, **kwargs): # real signature unknown
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is ''

__spec__ = None # (!) real value is ''

