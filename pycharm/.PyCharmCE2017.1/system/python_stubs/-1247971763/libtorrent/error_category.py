# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class error_category(__Boost_Python.instance):
    # no doc
    def message(self, error_category, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        message( (error_category)arg1, (int)arg2) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > message(boost::system::error_category {lvalue},int)
        """
        pass

    def name(self, error_category, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        name( (error_category)arg1) -> str :
        
            C++ signature :
                char const* name(boost::system::error_category {lvalue})
        """
        pass

    def __eq__(self, error_category, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (error_category)arg1, (error_category)arg2) -> object :
        
            C++ signature :
                _object* __eq__(boost::system::error_category {lvalue},boost::system::error_category)
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __lt__(self, error_category, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (error_category)arg1, (error_category)arg2) -> object :
        
            C++ signature :
                _object* __lt__(boost::system::error_category {lvalue},boost::system::error_category)
        """
        pass

    def __ne__(self, error_category, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (error_category)arg1, (error_category)arg2) -> object :
        
            C++ signature :
                _object* __ne__(boost::system::error_category {lvalue},boost::system::error_category)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass


