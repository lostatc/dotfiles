# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class session_flags_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    add_default_plugins = 1
    names = {
        'add_default_plugins': 1,
        'start_default_features': 2,
    }
    start_default_features = 2
    values = {
        1: 1,
        2: 2,
    }
    __slots__ = ()


