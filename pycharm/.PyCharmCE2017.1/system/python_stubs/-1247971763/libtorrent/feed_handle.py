# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class feed_handle(__Boost_Python.instance):
    # no doc
    def get_feed_status(self, feed_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_feed_status( (feed_handle)arg1) -> dict :
        
            C++ signature :
                boost::python::dict get_feed_status(libtorrent::feed_handle)
        """
        pass

    def settings(self, feed_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        settings( (feed_handle)arg1) -> dict :
        
            C++ signature :
                boost::python::dict settings(libtorrent::feed_handle {lvalue})
        """
        pass

    def set_settings(self, feed_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_settings( (feed_handle)arg1, (dict)arg2) -> None :
        
            C++ signature :
                void set_settings(libtorrent::feed_handle {lvalue},boost::python::dict)
        """
        pass

    def update_feed(self, feed_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        update_feed( (feed_handle)arg1) -> None :
        
            C++ signature :
                void update_feed(libtorrent::feed_handle {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    __instance_size__ = 32


