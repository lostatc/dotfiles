# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class enc_policy(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    disabled = 2
    enabled = 1
    forced = 0
    names = {
        'disabled': 2,
        'enabled': 1,
        'forced': 0,
        'pe_disabled': 2,
        'pe_enabled': 1,
        'pe_forced': 0,
    }
    pe_disabled = 2
    pe_enabled = 1
    pe_forced = 0
    values = {
        0: 0,
        1: 1,
        2: 2,
    }
    __slots__ = ()


