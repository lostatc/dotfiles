# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


from .torrent_alert import torrent_alert

class url_seed_alert(torrent_alert):
    # no doc
    def server_url(self, url_seed_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        server_url( (url_seed_alert)arg1) -> str :
        
            C++ signature :
                char const* server_url(libtorrent::url_seed_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    msg = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    url = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



