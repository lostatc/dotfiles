# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class create_torrent_flags_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    merkle = 2
    modification_time = 4
    names = {
        'merkle': 2,
        'modification_time': 4,
        'optimize': 1,
        'optimize_alignment': 1,
        'symlinks': 8,
    }
    optimize = 1
    optimize_alignment = 1
    symlinks = 8
    values = {
        1: 1,
        2: 2,
        4: 4,
        8: 8,
    }
    __slots__ = ()


