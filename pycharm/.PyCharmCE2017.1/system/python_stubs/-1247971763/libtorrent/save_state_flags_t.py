# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class save_state_flags_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    names = {
        'save_as_map': 64,
        'save_dht_proxy': 8,
        'save_dht_settings': 2,
        'save_dht_state': 4,
        'save_encryption_settings': 32,
        'save_i2p_proxy': 16,
        'save_peer_proxy': 8,
        'save_proxy': 8,
        'save_settings': 1,
        'save_tracker_proxy': 8,
        'save_web_proxy': 8,
    }
    save_as_map = 64
    save_dht_proxy = 8
    save_dht_settings = 2
    save_dht_state = 4
    save_encryption_settings = 32
    save_i2p_proxy = 16
    save_peer_proxy = 8
    save_proxy = 8
    save_settings = 1
    save_tracker_proxy = 8
    save_web_proxy = 8
    values = {
        1: 1,
        2: 2,
        4: 4,
        8: 8,
        16: 16,
        32: 32,
        64: 64,
    }
    __slots__ = ()


