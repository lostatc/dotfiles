# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


# Variables with simple values

create_metadata_plugin = 'metadata_transfer'

create_smart_ban_plugin = 'smart_ban'

create_ut_metadata_plugin = 'ut_metadata'

create_ut_pex_plugin = 'ut_pex'

version = '1.1.1.0'

version_major = 1
version_minor = 1

__version__ = '1.1.1.0'

# functions

def add_files(file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    add_files( (file_storage)fs, (str)path [, (int)flags=0]) -> None :
    
        C++ signature :
            void add_files(libtorrent::file_storage {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > [,unsigned int=0])
    
    add_files( (file_storage)fs, (str)path, (object)predicate [, (int)flags=0]) -> None :
    
        C++ signature :
            void add_files(libtorrent::file_storage {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,boost::python::api::object [,unsigned int=0])
    """
    pass

def add_magnet_uri(session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    add_magnet_uri( (session)arg1, (str)arg2, (dict)arg3) -> torrent_handle :
    
        C++ signature :
            libtorrent::torrent_handle add_magnet_uri(libtorrent::session {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,boost::python::dict)
    """
    pass

def bdecode(p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    bdecode( (object)arg1) -> object :
    
        C++ signature :
            libtorrent::entry bdecode(bytes)
    """
    pass

def bencode(p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    bencode( (object)arg1) -> object :
    
        C++ signature :
            bytes bencode(libtorrent::entry)
    """
    pass

def client_fingerprint(sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    client_fingerprint( (sha1_hash)arg1) -> object :
    
        C++ signature :
            boost::python::api::object client_fingerprint(libtorrent::sha1_hash)
    """
    pass

def generic_category(): # real signature unknown; restored from __doc__
    """
    generic_category() -> error_category :
    
        C++ signature :
            boost::system::error_category generic_category()
    """
    return error_category

def get_bdecode_category(): # real signature unknown; restored from __doc__
    """
    get_bdecode_category() -> error_category :
    
        C++ signature :
            boost::system::error_category {lvalue} get_bdecode_category()
    """
    return error_category

def get_http_category(): # real signature unknown; restored from __doc__
    """
    get_http_category() -> error_category :
    
        C++ signature :
            boost::system::error_category {lvalue} get_http_category()
    """
    return error_category

def get_i2p_category(): # real signature unknown; restored from __doc__
    """
    get_i2p_category() -> error_category :
    
        C++ signature :
            boost::system::error_category {lvalue} get_i2p_category()
    """
    return error_category

def get_libtorrent_category(): # real signature unknown; restored from __doc__
    """
    get_libtorrent_category() -> error_category :
    
        C++ signature :
            boost::system::error_category {lvalue} get_libtorrent_category()
    """
    return error_category

def get_socks_category(): # real signature unknown; restored from __doc__
    """
    get_socks_category() -> error_category :
    
        C++ signature :
            boost::system::error_category {lvalue} get_socks_category()
    """
    return error_category

def get_upnp_category(): # real signature unknown; restored from __doc__
    """
    get_upnp_category() -> error_category :
    
        C++ signature :
            boost::system::error_category {lvalue} get_upnp_category()
    """
    return error_category

def high_performance_seed(): # real signature unknown; restored from __doc__
    """
    high_performance_seed() -> session_settings :
    
        C++ signature :
            libtorrent::session_settings high_performance_seed()
    
    high_performance_seed( (object)arg1) -> None :
    
        C++ signature :
            void high_performance_seed(libtorrent::settings_pack {lvalue})
    """
    return session_settings

def identify_client(sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    identify_client( (sha1_hash)arg1) -> str :
    
        C++ signature :
            std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > identify_client(libtorrent::sha1_hash)
    """
    pass

def make_magnet_uri(torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    make_magnet_uri( (torrent_handle)arg1) -> str :
    
        C++ signature :
            std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > make_magnet_uri(libtorrent::torrent_handle)
    
    make_magnet_uri( (torrent_info)arg1) -> str :
    
        C++ signature :
            std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > make_magnet_uri(libtorrent::torrent_info)
    """
    pass

def min_memory_usage(): # real signature unknown; restored from __doc__
    """
    min_memory_usage() -> session_settings :
    
        C++ signature :
            libtorrent::session_settings min_memory_usage()
    
    min_memory_usage( (object)arg1) -> None :
    
        C++ signature :
            void min_memory_usage(libtorrent::settings_pack {lvalue})
    """
    return session_settings

def parse_magnet_uri(p_str, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    parse_magnet_uri( (str)arg1) -> dict :
    
        C++ signature :
            boost::python::dict parse_magnet_uri(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
    """
    pass

def set_piece_hashes(create_torrent, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    set_piece_hashes( (create_torrent)arg1, (str)arg2) -> None :
    
        C++ signature :
            void set_piece_hashes(libtorrent::create_torrent {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
    
    set_piece_hashes( (create_torrent)arg1, (str)arg2, (object)arg3) -> None :
    
        C++ signature :
            void set_piece_hashes(libtorrent::create_torrent {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,boost::python::api::object)
    """
    pass

def system_category(): # real signature unknown; restored from __doc__
    """
    system_category() -> error_category :
    
        C++ signature :
            boost::system::error_category system_category()
    """
    return error_category

# classes

from .alert import alert
from .torrent_alert import torrent_alert
from .add_torrent_alert import add_torrent_alert
from .add_torrent_params_flags_t import add_torrent_params_flags_t
from .announce_entry import announce_entry
from .anonymous_mode_alert import anonymous_mode_alert
from .bandwidth_mixed_algo_t import bandwidth_mixed_algo_t
from .sha1_hash import sha1_hash
from .peer_id import peer_id
from .big_number import big_number
from .peer_alert import peer_alert
from .block_downloading_alert import block_downloading_alert
from .block_finished_alert import block_finished_alert
from .block_timeout_alert import block_timeout_alert
from .cache_status import cache_status
from .choking_algorithm_t import choking_algorithm_t
from .create_torrent import create_torrent
from .create_torrent_flags_t import create_torrent_flags_t
from .deadline_flags import deadline_flags
from .dht_announce_alert import dht_announce_alert
from .dht_get_peers_alert import dht_get_peers_alert
from .dht_get_peers_reply_alert import dht_get_peers_reply_alert
from .dht_immutable_item_alert import dht_immutable_item_alert
from .dht_lookup import dht_lookup
from .dht_mutable_item_alert import dht_mutable_item_alert
from .dht_outgoing_get_peers_alert import dht_outgoing_get_peers_alert
from .dht_put_alert import dht_put_alert
from .tracker_alert import tracker_alert
from .dht_reply_alert import dht_reply_alert
from .dht_settings import dht_settings
from .dht_stats_alert import dht_stats_alert
from .disk_cache_algo_t import disk_cache_algo_t
from .enc_level import enc_level
from .enc_policy import enc_policy
from .error_category import error_category
from .error_code import error_code
from .external_ip_alert import external_ip_alert
from .fastresume_rejected_alert import fastresume_rejected_alert
from .feed_handle import feed_handle
from .file_completed_alert import file_completed_alert
from .file_entry import file_entry
from .file_error_alert import file_error_alert
from .file_flags_t import file_flags_t
from .file_progress_flags import file_progress_flags
from .file_renamed_alert import file_renamed_alert
from .file_rename_failed_alert import file_rename_failed_alert
from .file_slice import file_slice
from .file_storage import file_storage
from .fingerprint import fingerprint
from .hash_failed_alert import hash_failed_alert
from .i2p_alert import i2p_alert
from .incoming_connection_alert import incoming_connection_alert
from .invalid_request_alert import invalid_request_alert
from .io_buffer_mode_t import io_buffer_mode_t
from .ip_filter import ip_filter
from .kind import kind
from .listen_failed_alert import listen_failed_alert
from .listen_on_flags_t import listen_on_flags_t
from .listen_succeeded_alert import listen_succeeded_alert
from .lsd_error_alert import lsd_error_alert
from .metadata_failed_alert import metadata_failed_alert
from .metadata_received_alert import metadata_received_alert
from .move_flags_t import move_flags_t
from .options_t import options_t
from .pause_flags_t import pause_flags_t
from .peer_ban_alert import peer_ban_alert
from .peer_blocked_alert import peer_blocked_alert
from .peer_connect_alert import peer_connect_alert
from .peer_disconnected_alert import peer_disconnected_alert
from .peer_error_alert import peer_error_alert
from .peer_info import peer_info
from .peer_request import peer_request
from .peer_snubbed_alert import peer_snubbed_alert
from .peer_unsnubbed_alert import peer_unsnubbed_alert
from .performance_alert import performance_alert
from .performance_warning_t import performance_warning_t
from .pe_settings import pe_settings
from .piece_finished_alert import piece_finished_alert
from .portmap_alert import portmap_alert
from .portmap_error_alert import portmap_error_alert
from .protocol_type import protocol_type
from .proxy_settings import proxy_settings
from .proxy_type import proxy_type
from .read_piece_alert import read_piece_alert
from .request_dropped_alert import request_dropped_alert
from .save_resume_data_alert import save_resume_data_alert
from .save_resume_data_failed_alert import save_resume_data_failed_alert
from .save_resume_flags_t import save_resume_flags_t
from .save_state_flags_t import save_state_flags_t
from .scrape_failed_alert import scrape_failed_alert
from .scrape_reply_alert import scrape_reply_alert
from .seed_choking_algorithm_t import seed_choking_algorithm_t
from .session import session
from .session_flags_t import session_flags_t
from .session_settings import session_settings
from .session_stats_alert import session_stats_alert
from .session_status import session_status
from .state_changed_alert import state_changed_alert
from .state_update_alert import state_update_alert
from .stats_alert import stats_alert
from .stats_channel import stats_channel
from .status_flags_t import status_flags_t
from .storage_mode_t import storage_mode_t
from .storage_moved_alert import storage_moved_alert
from .storage_moved_failed_alert import storage_moved_failed_alert
from .suggest_mode_t import suggest_mode_t
from .torrent_added_alert import torrent_added_alert
from .torrent_checked_alert import torrent_checked_alert
from .torrent_deleted_alert import torrent_deleted_alert
from .torrent_delete_failed_alert import torrent_delete_failed_alert
from .torrent_error_alert import torrent_error_alert
from .torrent_finished_alert import torrent_finished_alert
from .torrent_handle import torrent_handle
from .torrent_info import torrent_info
from .torrent_need_cert_alert import torrent_need_cert_alert
from .torrent_paused_alert import torrent_paused_alert
from .torrent_removed_alert import torrent_removed_alert
from .torrent_resumed_alert import torrent_resumed_alert
from .torrent_status import torrent_status
from .torrent_update_alert import torrent_update_alert
from .tracker_announce_alert import tracker_announce_alert
from .tracker_error_alert import tracker_error_alert
from .tracker_reply_alert import tracker_reply_alert
from .tracker_source import tracker_source
from .tracker_warning_alert import tracker_warning_alert
from .udp_error_alert import udp_error_alert
from .unwanted_block_alert import unwanted_block_alert
from .url_seed_alert import url_seed_alert
# variables with complex values

__loader__ = None # (!) real value is ''

__spec__ = None # (!) real value is ''

