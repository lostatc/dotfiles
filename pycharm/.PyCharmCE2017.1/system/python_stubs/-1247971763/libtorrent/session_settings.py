# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class session_settings(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    active_dht_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    active_downloads = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    active_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    active_lsd_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    active_seeds = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    active_tracker_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    alert_queue_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    allowed_fast_set_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    allow_i2p_mixed = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    allow_multiple_connections_per_ip = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    allow_reordered_disk_operations = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    always_send_user_agent = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    announce_double_nat = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    announce_ip = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    announce_to_all_tiers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    announce_to_all_trackers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    anonymous_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    apply_ip_filter_to_trackers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    auto_manage_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    auto_manage_prefer_seeds = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    auto_manage_startup = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    auto_scrape_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    auto_scrape_min_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ban_web_seeds = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    broadcast_lsd = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    cache_buffer_chunk_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    cache_expiry = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    cache_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    choking_algorithm = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    close_redundant_connections = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    coalesce_reads = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    coalesce_writes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    connections_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    connection_speed = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    decrease_est_reciprocation_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    default_cache_min_age = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    default_est_reciprocation_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_announce_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_upload_rate_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    disable_hash_checks = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    disk_cache_algorithm = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    disk_io_read_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    disk_io_write_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dont_count_slow_torrents = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    download_rate_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    drop_skipped_requests = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    enable_incoming_tcp = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    enable_incoming_utp = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    enable_outgoing_tcp = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    enable_outgoing_utp = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    explicit_cache_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    explicit_read_cache = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    file_checks_delay_per_block = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    file_pool_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    force_proxy = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    free_torrent_hashes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    guided_read_cache = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    half_open_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    handshake_client_version = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    handshake_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ignore_limits_on_local_network = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ignore_resume_timestamps = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    inactivity_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    incoming_starts_queued_torrents = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    increase_est_reciprocation_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    initial_picker_threshold = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    lazy_bitfields = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    listen_queue_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    local_download_rate_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    local_service_announce_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    local_upload_rate_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    lock_disk_cache = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    lock_files = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    low_prio_disk = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_allowed_in_request_queue = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_failcount = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_http_recv_buffer_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_metadata_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_out_request_queue = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_paused_peerlist_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_peerlist_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_pex_peers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_queued_disk_bytes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_queued_disk_bytes_low_watermark = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_rejects = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_sparse_regions = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_suggest_pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    min_announce_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    min_reconnect_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    mixed_mode_algorithm = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    no_atime_storage = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    no_connect_privileged_ports = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    no_recheck_incomplete_resume = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_optimistic_unchoke_slots = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_want = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    optimistic_disk_retry = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    optimistic_unchoke_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    optimize_hashing_for_speed = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    peer_connect_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    peer_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    peer_tos = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    peer_turnover = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    peer_turnover_cutoff = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    peer_turnover_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    piece_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    prefer_udp_trackers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    prioritize_partial_pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    rate_limit_ip_overhead = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    rate_limit_utp = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    read_cache_line_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    read_job_every = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    recv_socket_buffer_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    report_redundant_bytes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    report_true_downloaded = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    report_web_seed_downloads = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    request_queue_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    request_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seeding_outgoing_connections = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seeding_piece_quota = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seed_choking_algorithm = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seed_time_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seed_time_ratio_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    send_buffer_low_watermark = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    send_buffer_watermark = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    send_buffer_watermark_factor = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    send_redundant_have = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    send_socket_buffer_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    share_mode_target = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    share_ratio_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    smooth_connects = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ssl_listen = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    stop_tracker_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    strict_end_game_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    strict_super_seeding = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    support_merkle_torrents = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    support_share_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tick_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    torrent_connect_boost = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tracker_backoff = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tracker_completion_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tracker_maximum_response_length = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tracker_receive_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    udp_tracker_token_expiry = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    unchoke_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    unchoke_slots_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upload_rate_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upnp_ignore_nonrouters = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    urlseed_pipeline_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    urlseed_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    urlseed_wait_retry = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    user_agent = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    use_dht_as_fallback = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    use_disk_cache_pool = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    use_disk_read_ahead = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    use_parole_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    use_read_cache = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_connect_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_delayed_ack = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_dynamic_sock_buf = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_fin_resends = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_gain_factor = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_loss_multiplier = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_min_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_num_resends = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_syn_resends = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_target_delay = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    volatile_read_cache = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    whole_pieces_threshold = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    write_cache_line_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 768


