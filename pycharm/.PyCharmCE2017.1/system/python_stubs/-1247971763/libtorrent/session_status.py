# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class session_status(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    active_requests = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    allowed_upload_slots = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_download_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_global_nodes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_nodes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_node_cache = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_torrents = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_total_allocations = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_upload_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    download_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    down_bandwidth_bytes_queue = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    down_bandwidth_queue = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    has_incoming_connections = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ip_overhead_download_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ip_overhead_upload_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_peers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_unchoked = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    optimistic_unchoke_counter = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    payload_download_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    payload_upload_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_dht_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_dht_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_failed_bytes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_ip_overhead_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_ip_overhead_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_payload_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_payload_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_redundant_bytes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_tracker_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_tracker_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tracker_download_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tracker_upload_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    unchoke_counter = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upload_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    up_bandwidth_bytes_queue = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    up_bandwidth_queue = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    utp_stats = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 424


