# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class announce_entry(__Boost_Python.instance):
    # no doc
    def can_announce(self, announce_entry, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        can_announce( (announce_entry)arg1, (object)arg2, (bool)arg3) -> bool :
        
            C++ signature :
                bool can_announce(libtorrent::announce_entry {lvalue},std::chrono::time_point<std::chrono::_V2::system_clock, std::chrono::duration<long, std::ratio<1l, 1000000000l> > >,bool)
        """
        pass

    def is_working(self, announce_entry, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_working( (announce_entry)arg1) -> bool :
        
            C++ signature :
                bool is_working(libtorrent::announce_entry {lvalue})
        """
        pass

    def reset(self, announce_entry, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reset( (announce_entry)arg1) -> None :
        
            C++ signature :
                void reset(libtorrent::announce_entry {lvalue})
        """
        pass

    def trim(self, announce_entry, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        trim( (announce_entry)arg1) -> None :
        
            C++ signature :
                void trim(libtorrent::announce_entry {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (str)arg2) -> None :
        
            C++ signature :
                void __init__(_object*,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    complete_sent = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    fails = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    fail_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    send_stats = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    source = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    start_sent = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tier = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    updating = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    url = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    verified = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 168


