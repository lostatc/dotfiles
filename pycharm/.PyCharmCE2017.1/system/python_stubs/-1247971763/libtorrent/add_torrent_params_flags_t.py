# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class add_torrent_params_flags_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    flag_apply_ip_filter = 16
    flag_auto_managed = 64
    flag_duplicate_is_error = 128
    flag_merge_resume_http_seeds = 32768
    flag_merge_resume_trackers = 256
    flag_override_resume_data = 2
    flag_paused = 32
    flag_seed_mode = 1
    flag_sequential_download = 2048
    flag_share_mode = 8
    flag_stop_when_ready = 16384
    flag_super_seeding = 1024
    flag_update_subscribe = 512
    flag_upload_mode = 4
    flag_use_resume_save_path = 4096
    names = {
        'flag_apply_ip_filter': 16,
        'flag_auto_managed': 64,
        'flag_duplicate_is_error': 128,
        'flag_merge_resume_http_seeds': 32768,
        'flag_merge_resume_trackers': 256,
        'flag_override_resume_data': 2,
        'flag_paused': 32,
        'flag_seed_mode': 1,
        'flag_sequential_download': 2048,
        'flag_share_mode': 8,
        'flag_stop_when_ready': 16384,
        'flag_super_seeding': 1024,
        'flag_update_subscribe': 512,
        'flag_upload_mode': 4,
        'flag_use_resume_save_path': 4096,
    }
    values = {
        1: 1,
        2: 2,
        4: 4,
        8: 8,
        16: 16,
        32: 32,
        64: 64,
        128: 128,
        256: 256,
        512: 512,
        1024: 1024,
        2048: 2048,
        4096: 4096,
        16384: 16384,
        32768: 32768,
    }
    __slots__ = ()


