# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class alert(__Boost_Python.instance):
    # no doc
    def category(self, alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        category( (alert)arg1) -> int :
        
            C++ signature :
                int category(libtorrent::alert {lvalue})
        """
        pass

    def message(self, alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        message( (alert)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > message(libtorrent::alert {lvalue})
        """
        pass

    def severity(self, alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        severity( (alert)arg1) -> severity_levels :
        
            C++ signature :
                libtorrent::alert::severity_t severity(libtorrent::alert {lvalue})
        """
        pass

    def what(self, alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        what( (alert)arg1) -> str :
        
            C++ signature :
                char const* what(libtorrent::alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __str__(self, alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (alert)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(libtorrent::alert {lvalue})
        """
        pass

    category_t = None # (!) real value is ''
    severity_levels = None # (!) real value is ''


