# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class dht_settings(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    aggressive_lookups = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    block_ratelimit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    block_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    enforce_node_id = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    extended_routing_table = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ignore_dark_internet = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    item_lifetime = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_dht_items = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_fail_count = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_peers_reply = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_torrents = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_torrent_search_reply = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    privacy_lookups = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    read_only = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    restrict_routing_ips = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    restrict_search_ips = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    search_branching = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    service_port = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


