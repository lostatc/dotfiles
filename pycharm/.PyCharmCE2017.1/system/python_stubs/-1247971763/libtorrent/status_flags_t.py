# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class status_flags_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    names = {
        'query_accurate_download_counters': 2,
        'query_distributed_copies': 1,
        'query_last_seen_complete': 4,
        'query_pieces': 8,
        'query_verified_pieces': 16,
    }
    query_accurate_download_counters = 2
    query_distributed_copies = 1
    query_last_seen_complete = 4
    query_pieces = 8
    query_verified_pieces = 16
    values = {
        1: 1,
        2: 2,
        4: 4,
        8: 8,
        16: 16,
    }
    __slots__ = ()


