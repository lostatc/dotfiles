# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class bandwidth_mixed_algo_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    names = {
        'peer_proportional': 1,
        'prefer_tcp': 0,
    }
    peer_proportional = 1
    prefer_tcp = 0
    values = {
        0: 0,
        1: 1,
    }
    __slots__ = ()


