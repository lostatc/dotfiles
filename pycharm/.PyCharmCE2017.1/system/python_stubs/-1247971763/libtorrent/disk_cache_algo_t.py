# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.6/site-packages/libtorrent.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import Boost.Python as __Boost_Python


class disk_cache_algo_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    avoid_readback = 2
    largest_contiguous = 1
    lru = 0
    names = {
        'avoid_readback': 2,
        'largest_contiguous': 1,
        'lru': 0,
    }
    values = {
        0: 0,
        1: 1,
        2: 2,
    }
    __slots__ = ()


