# encoding: utf-8
# module keyutils._keyutils
# from /usr/lib/python3.6/site-packages/keyutils/_keyutils.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc

# imports
import builtins as __builtins__ # <module 'builtins' (built-in)>

# functions

def add_key(*args, **kwargs): # real signature unknown
    pass

def clear(*args, **kwargs): # real signature unknown
    pass

def join_session_keyring(*args, **kwargs): # real signature unknown
    pass

def link(*args, **kwargs): # real signature unknown
    pass

def read_key(*args, **kwargs): # real signature unknown
    pass

def request_key(*args, **kwargs): # real signature unknown
    pass

def revoke(*args, **kwargs): # real signature unknown
    pass

def search(*args, **kwargs): # real signature unknown
    pass

def set_timeout(*args, **kwargs): # real signature unknown
    pass

def unlink(*args, **kwargs): # real signature unknown
    pass

# classes

class constants(object):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    EKEYEXPIRED = 127
    EKEYREJECTED = 129
    EKEYREVOKED = 128
    ENOKEY = 126
    KEY_SPEC_PROCESS_KEYRING = -2
    KEY_SPEC_SESSION_KEYRING = -3
    KEY_SPEC_THREAD_KEYRING = -1
    KEY_SPEC_USER_KEYRING = -4
    KEY_SPEC_USER_SESSION_KEYRING = -5
    __dict__ = None # (!) real value is ''


class error(Exception):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values

__loader__ = None # (!) real value is ''

__spec__ = None # (!) real value is ''

__test__ = {}

