# encoding: utf-8
# module _ast27
# from /usr/lib/python3.6/site-packages/_ast27.cpython-36m-x86_64-linux-gnu.so
# by generator 1.145
# no doc
# no imports

from .slice import slice

class Slice(slice):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    _fields = (
        'lower',
        'upper',
        'step',
    )


