# encoding: utf-8
# module _orcus
# from /usr/lib/python3.6/site-packages/_orcus.so
# by generator 1.145
# no doc
# no imports

# functions

def info(*args, **kwargs): # real signature unknown
    """ Print orcus module information. """
    pass

def _xlsx_read_file(*args, **kwargs): # real signature unknown
    """ Load specified xlsx file into a document model. """
    pass

# classes

class Document(object):
    """ orcus document object """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    sheets = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """sheet objects"""



class Sheet(object):
    """ orcus sheet object """
    def get_rows(self, *args, **kwargs): # real signature unknown
        """ Get a sheet row iterator. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    data_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """data size"""

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """sheet name"""

    sheet_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """sheet size"""



class SheetRows(object):
    """ orcus sheet rows iterator """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __iter__(self, *args, **kwargs): # real signature unknown
        """ Implement iter(self). """
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __next__(self, *args, **kwargs): # real signature unknown
        """ Implement next(self). """
        pass


# variables with complex values

__loader__ = None # (!) real value is ''

__spec__ = None # (!) real value is ''

