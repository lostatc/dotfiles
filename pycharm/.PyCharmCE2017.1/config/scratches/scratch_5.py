#!/usr/bin/env python3

import re
from typing import Tuple, Union, Optional, Iterable

ANSI_COLORS = {
    "black": 0, "red": 1, "green": 2, "yellow": 3, "blue": 4, "magenta": 5,
    "cyan": 6, "white": 7}
ANSI_STYLES = {
    "bold": 1, "underline": 4}

def _ansi_join(*args):
    """Convert values to strings and join with semicolons."""
    return ";".join(str(value) for value in args)

def _get_color_code(spec: Union[str, int], base: int):
    """Get the appropriate ansi color code based on input.

    Args:
        spec: The color specification to be parsed.
        base: The base value for color encoding.

    Returns:
        The ANSI color code.
    """
    if isinstance(spec, int) and 0 <= spec <= 255:
        return _ansi_join(base + 8, 5, spec)
    elif isinstance(spec, str):
        spec = spec.strip().lower()
        hex_match = re.search(r"^#([0-9a-f]{6})$", spec)

        if spec in ANSI_COLORS:
            return _ansi_join(base + ANSI_COLORS[spec])
        elif hex_match:
            hex_code = hex_match.group(1)
            rgb = tuple(
                int(hex_code[digit:digit + 2], 16) for digit in range(0, 6, 2))
            return _ansi_join(base + 8, 2, _ansi_join(*rgb))

    raise ValueError("unrecognized color spec '{0}'".format(str(spec)))

def ansi_format(
        fg: Union[str, int, None], bg: Union[str, int, None],
        style: Union[str, Iterable[str], None]) -> Tuple[str, str]:
    """Get the appropriate ANSI escape sequences based on input.

    Args:
        fg: The foreground color or hex code.
        bg: The background color or hex code.
        style: The text style.

    Returns:
        A tuple containing the starting and ending ANSI escape sequences.
    """
    start_codes = []
    if fg:
        start_codes.append(_get_color_code(fg, 30))
    if bg:
        start_codes.append(_get_color_code(bg, 40))
    if style:
        if isinstance(style, str):
            styles = [style]
        else:
            styles = list(style)

        for part in styles:
            if part in ANSI_STYLES:
                start_codes.append(ANSI_STYLES[part])
            else:
                raise ValueError("unrecognized style '{0}'".format(part))

    return "\x1b[{0}m".format(_ansi_join(*start_codes)), "\x1b[0m"

start, end = ansi_format("#01b7f8", "blue", ["bold", "underline"])
