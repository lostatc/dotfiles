from docutils import nodes

def _apply_markup(text, markup_spans):
    def parse_top_level(markup_spans, parent_span):
        # Get top-level spans and include spans without any markup.
        prev_end = parent_span[0]
        top_level_spans = []
        for (start, end), markup_type in markup_spans:
            if start < prev_end or (start, end) == parent_span:
                continue

            top_level_spans.append(((prev_end, start), None))
            top_level_spans.append(((start, end), markup_type))

            prev_end = end

        # Include text after the last markup section.
        top_level_spans.append(((prev_end, parent_span[1]), None))

        # Create nodes from those spans.
        top_level_nodes = []
        for (start, end), markup_type in top_level_spans:
            if markup_type is None:
                top_level_nodes.append(nodes.Text(text[start:end]))
            elif markup_type == "strong":
                top_level_nodes.append(nodes.strong())
            elif markup_type == "em":
                top_level_nodes.append(nodes.emphasis())

        for i, ((start, end), markup_type) in enumerate(top_level_spans):
            if markup_type is None:
                continue

            nested_spans = []
            for (nested_start, nested_end), nested_markup_type in markup_spans:
                if (nested_start in range(start, end)
                        and nested_end in range(start, end + 1)
                        and (nested_start, nested_end) != parent_span):
                    nested_spans.append(((nested_start, nested_end), nested_markup_type))


            top_level_nodes[i] += parse_top_level(nested_spans, (start, end))


        return top_level_nodes

    root = nodes.paragraph()
    for node in parse_top_level(markup_spans, (0, len(text))):
        root += node

    return root


cheat = "This text *is **too* long** and ***also** *much* too* short."
text = "This text is too long and also much too short."
#                 ^         ^         ^         ^
markup_spans = [
    ((10, 16), "em"), ((13, 21), "strong"), ((26, 39), "em"),
    ((26, 30), "strong"), ((31, 35), "em")]
spans = [
    ((0, 10), None), ((10, 16), "em"), ((10, 13), None), ((13, 21), "strong"), ((21, 26), None),
    ((26, 30), "strong"), ((26, 34), "em"), ((34, 41), None)]

print(_apply_markup(text, markup_spans))
