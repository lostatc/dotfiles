#!/usr/bin/env python3

import types
import re

from colors import color

from linotype.items import MarkupPositions

ANSI_REGEX = re.compile(r"\x1b[^m]*m")

strong = [None, None, "bold"]
em = [None, None, "underline"]

text = "This text is too long and also much too short and stuff."
#                 ^         ^         ^         ^
positions = MarkupPositions([("too", 1), ("too short", 0)], [("also", 0)])
markup_spans = [((26, 30), "em"), ((36, 39), "strong"), ((36, 45), "em")]

markup_spans.sort(key=lambda x: x[0][1], reverse=True)
markup_spans.sort(key=lambda x: x[0][0])

# Get the positions of ANSI escape sequences in the text. Keep track of
# which sequences are still "open" so that the ends of other sequences don't
# close them.
markup_sequences = []
open_sequences = []
for (start, end), markup_type in markup_spans:
    start_sequence, end_sequence = ANSI_REGEX.findall(
        color("", *locals()[markup_type]))

    open_sequences = [
        (position, sequence) for position, sequence in open_sequences
        if position > start]

    markup_sequences.append((start, start_sequence))
    markup_sequences.append((end, end_sequence))
    markup_sequences += [
        (end, sequence) for position, sequence in open_sequences]

    open_sequences.append((end, start_sequence))

markup_sequences.sort(key=lambda x: x[0])

# Get a list of strings that will make up the output. This is the ordered
# ANSI escape sequences with the appropriate text between them.
prev_position = 0
text_sequences = []
for position, sequence in markup_sequences:
    text_sequences.append(text[prev_position:position])
    text_sequences.append(sequence)
    prev_position = position
text_sequences.append(text[prev_position:])

print("".join(text_sequences))
