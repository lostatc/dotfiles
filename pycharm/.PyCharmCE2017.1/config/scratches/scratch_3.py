#!/usr/bin/env python3

import functools


class MyClass:
    def __init__(self):
        self.color = "red"

    def change_color(self, color):
        self.color = color



class ClientClass:

    def __init__(self):
        self.mc = MyClass()
        self.to_blue = functools.partial(self.mc.change_color, "blue")

    @property
    def color(self):
        return self.mc.color

cc = ClientClass()
cc.to_blue()
print(cc.color)