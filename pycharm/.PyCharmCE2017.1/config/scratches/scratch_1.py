#!/usr/bin/env python3

import re
from typing import NamedTuple, List, Tuple

from docutils.frontend import OptionParser
from docutils import nodes
from docutils.parsers.rst import Parser
from docutils.parsers.rst.states import Inliner

from linotype.items import MARKUP_CHARS

def _apply_markup(text, spans):
    spans.sort(key=lambda x: x[0][0])

    root_node = nodes.paragraph()
    parent_node = root_node
    parent_span = (0, len(text))
    previous_span = (0, 0)

    ancestor_nodes = collections.deque([parent_node])
    ancestor_spans = collections.deque([parent_span])

    for (start, end), markup_type in spans:
        parent_start, parent_end = parent_span
        prev_start, prev_end = previous_span

        if markup_type is None:
            # The current span has no markup.
            parent_node = root_node
            root_node.append(nodes.Text(text[start:end]))
        elif start < prev_end < end:
            # The current span overlaps with a previous span.
            root_node.append(nodes.Text(text[prev_end:end]))
        else:
            if markup_type == "strong":
                node_type = nodes.strong
            elif markup_type == "em":
                node_type = nodes.emphasis

            if start >= parent_start and end <= parent_end:
                # The current span is within the parent span.
                new_node = node_type("", nodes.Text(text[start:end]))

                # Remove the content of the current node from the parent node.
                parent_node[-1] = nodes.Text(text[prev_start:start])

                ancestor_nodes.append(new_node)
                ancestor_spans.append((start, end))
                parent_node += new_node
                parent_node = parent_node[-1]
            else:
                # The current span is not within the parent span.
                new_node = node_type("", nodes.Text(text[start:end]))

                parent_node = ancestor_nodes.pop()
                parent_span = ancestor_spans.pop()
                parent_start, parent_end = parent_span

                while start >= parent_start and end <= parent_end:
                    # The current span is within the parent span.
                    parent_node = ancestor_nodes.pop()
                    parent_span = ancestor_spans.pop()
                    parent_start, parent_end = parent_span

                parent_node += new_node
                parent_span = (start, end)

        previous_span = (start, end)

    return root_node.children
#                 V         V         V
text = "This text is too long and also too short."
cheat = "This text *is **too* long** and *also **too*** short."
spans = [
    ((0, 10), None), ((10, 16), "em"), ((13, 21), 'strong'), ((21, 26), None),
    ((26, 34), 'em'), ((31, 34), 'strong'), ((34, 41), None)]

print(_apply_markup(text, spans))

